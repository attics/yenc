<?php

namespace attics\Usenet\yenc;

class yenc
{
    /**
     * yDecodes an encoded string and returns it as a string.
     *
     * @param string $string yEncoded string
     * @return status
     * @throws Exception
     */
    function validate($string)
    {
        $encoded = array();

        // Extract the yEnc string itself.
        preg_match("/^(=ybegin.+=yend[^$]+?)$/ims", $string, $encoded);
        if (!isset($encoded[1])) {
            throw new Exception(Exception::YENC_NOT_FOUND);
        }


        unset($string);

        // Split lines
        $lines = explode("\r\n",$encoded[1]);

        if(count($lines)<4){
            throw new Exception(Exception::YENC_NOT_FOUND);
        }

        // Get header
        $first = array_shift($lines);
        $ybegin = $this->parse_meta_line($first);
        if (!isset($ybegin['ybegin'])) {
            throw new Exception(Exception::BEGIN_NOT_FOUND);
        }
        if (empty($ybegin['size'])) {
            throw new Exception(Exception::HEADER_SIZE_NOT_FOUND, $first);
        }

        // Get header second line if available
        $second = array_shift($lines);
        $ypart = $this->parse_meta_line($second);
        if (!isset($ypart['ypart'])) {
            throw new Exception(Exception::PART_NOT_FOUND);
        }
        if (empty($ypart['end'])) {
            throw new Exception(Exception::HEADER_SIZE_NOT_FOUND, $second);
        }

        // Get tail
        $last = array_pop($lines);
        $yend = $this->parse_meta_line($last);
        if (!empty($yend['yend'])) {
            throw new Exception(Exception::END_NOT_FOUND);
        }

        if (empty($yend['pcrc32'])) {
            throw new Exception(Exception::FOOTER_CRC_NOT_FOUND, $last);
        } else {
            $crc_part = $yend['pcrc32'];
        }

        if (empty($yend['size'])) {
            throw new Exception(Exception::HEADER_SIZE_NOT_FOUND, $last);
        }

        $encoded = implode(null,$lines);

        // Decode
        $decoded = '';
        for ($i = 0; $i < strlen($encoded); $i++) {
            if ($encoded{$i} == "=") {
                $i++;
                $decoded .= chr((ord($encoded{$i}) - 64) - 42);
            } else {
                $decoded .= chr(ord($encoded{$i}) - 42);
            }
        }

        // Make sure the decoded filesize is the same as the size specified in the header.
        $size_decoded = strlen($decoded);
        if ($size_decoded != $yend['size']) {
            throw new Exception(Exception::SIZE_DIFFERS, 'Meta:' . $yend['size'] . ' vs Decoded:' . $size_decoded);
        }

        // Check the CRC value
        $crc_decoded = dechex(crc32($decoded));
        $crc_part = dechex(hexdec($crc_part)); // Convert to platform independent HEX value
        if ($crc_part != $crc_decoded) {
            throw new Exception(Exception::CRC_DIFFERS, $last . ' : ' . $crc_decoded);
        }

        return new status($ybegin, $ypart, $yend);
    }

    /**
     * Parse yEnc meta data into array
     * @param $string
     * @return array|null
     */
    private function parse_meta_line($string)
    {
        if (empty($string)) {
            return null;
        }

        $string = trim($string, " \t\n\r\0\x0B=");
        $string = preg_replace('~\s+~', '&', $string);
        if (empty($string)) {
            return null;
        }

        // @TODO: spaces in name field ybegin&part=5&line=128&size=113999548&name=TVB_Mind&Hunter_10.mkv.vol155+148.PAR2

        $data = array();
        parse_str($string, $data);
        if (count($data) > 0) {
            return $data;
        }

        $vars = array();
        foreach ($data as $key => $value) {
            $vars[strtolower($key)] = $value;
        }

        return $vars;
    }
}


/**
 * yEncodes a string and returns it.
 *
 * @param string String to encode.
 * @param filename Name to use as the filename in the yEnc header (this
 *   does not have to be an actual file).
 * @param linelen Line length to use (can be up to 254 characters).
 * @param crc32 Set to <i>true</i> to include a CRC checksum in the
 *   trailer to allow decoders to verify data integrity.
 * @return yEncoded string or <i>false</i> on error.
 * @see decode()
 */
/*
function encode($string, $filename, $linelen = 128, $crc32 = true)
{
    // yEnc 1.3 draft doesn't allow line lengths of more than 254 bytes.
    if ($linelen > 254)
        $linelen = 254;

    if ($linelen < 1) {
        $this->error = "$linelen is not a valid line length.";
        return false;
    }

    // Encode each character of the string one at a time.
    for ($i = 0; $i < strlen($string); $i++) {
        $value = (ord($string{$i}) + 42) % 256;

        // Escape NULL, TAB, LF, CR, space, . and = characters.
        if ($value == 0 || $value == 9 || $value == 10 || $value == 13 || $value == 32 || $value == 46 || $value == 61)
            $encoded .= "=" . chr(($value + 64) % 256);
        else
            $encoded .= chr($value);
    }

    // Wrap the lines to $linelen characters
    // TODO: Make sure we don't split escaped characters in half, as per the yEnc spec.
    $encoded = trim(chunk_split($encoded, $linelen));

    // Tack a yEnc header onto the encoded string.
    $encoded = "=ybegin line=$linelen size=" . strlen($string) . " name=" . trim($filename) . "\r\n" . $encoded;
    $encoded .= "\r\n=yend size=" . strlen($string);

    // Add a CRC32 checksum if desired.
    if ($crc32 === true)
        $encoded .= " crc32=" . strtolower(sprintf("%04X", crc32($string)));

    return $encoded . "\r\n";
}

/**
 * yEncodes a file and returns it as a string.
 *
 * @param filename Full path and filename of the file to be encoded.
 *   This can also be a URL (http:// or ftp://).
 * @param linelen Line length to use (can be up to 254 characters).
 * @param crc32 Set to <i>true</i> to include a CRC checksum in the
 *   trailer to allow decoders to verify data integrity.
 * @return yEncoded file, or <i>false</i> on error.
 * @see decodeFile()
 */
/*
function encodeFile($filename, $linelen = 128, $crc32 = true)
{
    // Read the file into memory.
    if ($fp = @fopen($filename, "rb")) {
        while (!feof($fp))
            $file .= fread($fp, 8192);

        fclose($fp);

        // Encode the file.
        return $this->encode($file, $filename, $linelen, $crc32);
    } else {
        $this->error = "Could not open $filename for read access.";
        return false;
    }
}

/**
 * yDecodes an encoded file and writes the decoded file to the
 * specified directory, or returns it as a string if no directory is
 * specified.
 *
 * @param filename Full path and filename of the file to be decoded.
 * @param destination Destination directory where the decoded file will
 *   be written. This must be a valid directory <b>with no trailing
 *   slash</b> to which PHP has write access. If <i>destination</i> is
 *   not specified, the decoded file will be returned rather than
 *   written to the disk.
 * @return If <i>destination</i> is not set, the decoded file will be
 *   returned as a string. Otherwise, <i>true</i> will be returned on
 *   success. In either case, <i>false</i> will be returned on error.
 * @see encodeFile()
 */
/*
function decodeFile($filename, $destination = "")
{
    // Read the encoded file into memory.
    if ($fp = @fopen($filename, "rb")) {
        while (!feof($fp))
            $infile .= fread($fp, 8192);

        fclose($fp);

        // Send the file to the decoder.
        if ($out = $this->decode($infile, $destination)) {
            return $out;
        } else {
            // Decoding error.
            return false;
        }
    } else {
        $this->error = "Could not open $filename for read access.";
        return false;
    }
}
}
*/